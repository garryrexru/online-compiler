""" Модуль для формирования настроек для сервера."""

from aiohttp import web
from aiohttp_session import setup
from aiohttp_session.cookie_storage import EncryptedCookieStorage

from .route import setup_routes

__author__ = 'Пашуков И.С.'

__all__ = ['init_application']


async def init_application() -> web.Application:
    """
    Инициализация web-приложения.

    :return: web-приложение.
    """

    application = web.Application()
    application['websockets'] = list()

    setup(application,
          EncryptedCookieStorage(b'Thirty  two  sength  bytes  key.'))

    setup_routes(application)

    return application
