import React from "react";
import './App.css'
import dedent from 'dedent';
import Editor from 'react-simple-code-editor';
import { highlight, languages } from 'prismjs/components/prism-core';
import 'prismjs/components/prism-clike';
import 'prismjs/components/prism-javascript';
import 'prismjs/components/prism-python';
import 'prismjs/components/prism-css';
import 'prismjs/themes/prism-coy.css'



class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        code: dedent`
        # Online Python compiler (interpreter) to run Python online.
        # Write Python 3 code in this online editor and run it.
        print("Hello world")`,
        result: '',
    };
  }

  client = new WebSocket('ws://localhost:8080/ws');

  componentDidMount() {
    this.client.onmessage = (event) => {
      var msg = JSON.parse(event.data);
      var current_state = this.state;
      current_state['result'] = msg.result;
      this.setState({ current_state });
    };
} 

  handleRun = (event) => {
    var json_code = JSON.stringify(this.state.code);
    this.client.send(`{ "code" : ${json_code}}`);
    
    event.preventDefault();
  }

  handleClear = (event) => {
    var current_state = this.state
    current_state['result'] = '';
    this.setState({ current_state });

    event.preventDefault();
  }


  handleOnValueChanges = (event) => {
    let current_state = this.state;
    current_state['code'] = event;
    this.setState({ current_state });
  }

  render() {
    return (
      <div className="container">
        <div className='container__header'>
          <h1>Python Online Interpreter</h1>
        </div>
        <div className='container__content' >
          <div className='container__editor'>
            <div className='container__editor-title'>
              <div className='container__editor-name'>
                <p>main.py</p>
              </div>
              <button className='button' onClick={this.handleRun}>Run</button>
            </div>
            <div className="container_code-editor-area">
              <Editor
                placeholder="Type some code…"
                tabSize='4'
                value={this.state.code}
                onValueChange={code => this.handleOnValueChanges(code)}
                highlight={code => highlight(code, languages.python, 'python')}
                padding={15}
                className="container__code-editor" />
            </div>  
          </div>
          <div className='container__result'>
            <div className='container__result-title'>
              <div className='container__result-name'>
                  <p>Shell</p>
                </div>
                <button className='button' onClick={this.handleClear}>Clear</button>
              </div>
            <div className='container_code-result-area'>
              <Editor
                  value={this.state.result}
                  highlight={code => highlight(code, languages.css)}
                  padding={15}
                  disabled
                  className="container__code-result" />
            </div>
          </div>
        </div>  
      </div>
    );
  }
}

export default App;
