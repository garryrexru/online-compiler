""" Получение маршрутов для приложения."""

import aiohttp_cors

from aiohttp import web

from online_compiler.bl.websocket import WebSocket

__author__ = 'Пашуков И.С.'

__all__ = ['setup_routes']

# Список подключенных методов бизнес логики.
BUSINESS_LOGIC = [
    WebSocket
]

# Политика кросс-доменных запросов.
SAME_ORIGIN_POLICY = {
    "*": aiohttp_cors.ResourceOptions(
        allow_credentials=True,
        expose_headers="*",
        allow_headers="*"
    )
}


def setup_routes(application: web.Application) -> None:
    """
    Добавляем бизнес-логики в приложение.

    :param application: Web приложение.
    :return:
    """
    cors = aiohttp_cors.setup(application, defaults=SAME_ORIGIN_POLICY)
    for bl in BUSINESS_LOGIC:
        cors.add(application.router.add_route(*_get_route_from_bl(bl)))


def _get_route_from_bl(business_logic) -> tuple:
    """
    Получить из бизнес-логики маршрут для приложения.

    :param business_logic: Бизнес логика.
    :return: Список в корректном формате для построения маршрута.
    """
    return business_logic.METHOD, business_logic.ROUTE, business_logic
