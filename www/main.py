""" Точка входа для запуска приложения."""

import asyncio

from aiohttp import web

from online_compiler.core.web.init_application import init_application

__author__ = "Пашуков И.С."

__all__ = []



def main() -> None:
    """
    Точка входа для запуска web-приложения.
    Блокирующая функция.

    :return:
    """
    loop = asyncio.get_event_loop()
    application = loop.run_until_complete(init_application())

    web.run_app(application, host='localhost', port=8080)


if __name__ == '__main__':
    main()
