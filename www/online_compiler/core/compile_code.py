""" Выполняем внешний код."""

import sys
import contextlib
import traceback
from io import StringIO


__author__ = 'Пашуков И.С.'

__all__ = ['compile_code']


@contextlib.contextmanager
def stdoutIO(stdout=None):
    old = sys.stdout
    if stdout is None:
        stdout = StringIO()
    sys.stdout = stdout
    yield stdout
    sys.stdout = old


def compile_code(code):

    with stdoutIO() as s:
        try:
            exec(code)
            result = s.getvalue()
        except Exception:
            exp, val, tb = sys.exc_info()
            exc = traceback.format_exception(exp, val, tb)[2:]
            result = "".join(exc)

    return result
