""" Описание бизнес-логики."""

from aiohttp import web

__author__ = 'Пашуков И.С.'

__all__ = ['BusinessLogic']


class BusinessLogic(web.View):
    """ Метакласс для описания бизнес-логики."""

    # Маршрут.
    ROUTE = str()
    # Метод
    METHOD = str()

    async def get(self) -> web.StreamResponse:
        """
        Базовая реализация метода бизнес-логики.
        Если действия не заданы, то возвращает 501 код - 'Метод не поддерживается'.
        """
        return web.StreamResponse(status=501, reason='HTTPNotImplemented')

    async def post(self) -> web.StreamResponse:
        """
        Базовая реализация метода бизнес-логики.
        Если действия не заданы, то возвращаем 501 код - 'Метод не поддерживается'.
        """
        return web.StreamResponse(status=501, reason='HTTPNotImplemented')
