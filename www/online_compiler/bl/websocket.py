""" Получаем код из браузера. Отправляем код на выполнение, получаем выполненый код и возвращаем ответ с результатом."""

import asyncio
import concurrent
import json
import logging

from aiohttp import web, WSMsgType
from aiohttp_session import get_session

from online_compiler.core.compile_code import compile_code
from online_compiler.core.web.business_logic import BusinessLogic

__author__ = 'Пашуков И.С.'

__all__ = ['WebSocket']

logger = logging.getLogger("web_socket")


class WebSocket(BusinessLogic):
    """ Создаём вебсокет."""

    ROUTE = '/ws'
    METHOD = 'GET'

    async def get(self):
        """ Создаём вебсокет коннекцию."""
        ws = await self._get_ws_from_request()
        async for msg in ws:
            if msg.type == WSMsgType.TEXT:
                if msg.data == 'close':
                    await ws.close()
                else:
                    data = json.loads(msg.data)
                    code = data.get('code', None)

                    loop = asyncio.get_event_loop()
                    with concurrent.futures.ThreadPoolExecutor() as pool:
                        result = await loop.run_in_executor(pool, compile_code, code)

                    await ws.send_json({"result": result})

        return await self._close_ws(ws)

    async def _get_ws_from_request(self) -> web.WebSocketResponse:
        """ Получить вебсокет."""
        ws = web.WebSocketResponse()
        await ws.prepare(self.request)
        self.request.app['websockets'].append(ws)
        self.session = await get_session(self.request)
        return ws

    async def _close_ws(self, ws: web.WebSocketResponse) -> web.WebSocketResponse:
        """ Закрыть вебсокет."""
        self.request.app['websockets'].remove(ws)
        return ws
